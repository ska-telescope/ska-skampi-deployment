# SKA MVP Prototype Integration

SKAMPI was restructured into two parts - build/deploy and testing. This repository concerns building and deploying, using [ska-skampi](https://gitlab.com/ska-telescope/ska-skampi) to run tests as downstream pipelines. This separation brings the ability to quickly run a test, multiple times, with different test configurations and targetting different namespaces, on demand, having some separation between **low** and **mid**. In the pipelines of this repository, we already have jobs to trigger tests in on-demand deployments.

## CI/CD Pipeline

The CI/CD pipeline was refactored to be deployment-focused only, although, we still need continuous testing on the changes we do to the charts. The deployment jobs were re-organized by **datacentre**, to avoid problematic misclicks in the pipeline controls. Some important considerantions on the pipeline:

* **on-demand**_\<datacentre\> - This stage allows to deploy, redeploy, test and destroy a temporary environment. These environments can be deployed to run manual tests or explore with debugging tools
* **deploy+test+cleanup** - Deploys a test-environment to STFC-Techops (deploy-low and deploy-mid) and triggers test pipelines in [ska-skampi](https://gitlab.com/ska-telescope/ska-skampi) (test-low and test-mid), automatically cleaning up the environment upon success. The environment is left alive in the case of a failure, to allow debugging and investigation
* On the **main** branch, we can also deploy to the integration and staging environments. To deploy on integration, we must successfully deploy and test mid and low. To deploy to staging, integration deployment must succeed.

This guarantees that changes in the deployment (i.e., chart versions, default values) are tested against the latest tests (by default) in SKAMPI. One can set `SKA_SKAMPI_TESTS_REF` to a different value, in order to run a different tests branch.

Note that we can now trigger tests against an **on-demand** deployment multiple times. This can help us assert the idempotency of the test harness, and to simply do different test runs on live environments.

# Getting Started
[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-skampi-deployment/badge/?version=latest)](https://developer.skatelescope.org/projects/ska-skampi-deployment/en/latest/?badge=latest)

## Cloning the repository
Checkout the repository and ensure that submodules (including `.make`) are cloned with:
```
$ git clone --recurse-submodules git@gitlab.com:ska-telescope/ska-skampi-deployment.git
$ cd ska-skampi-deployment
```

## Setting up the local Python environment
After checking out the repository, and making sure **Python** and **poetry** are present, do:
```
$ poetry install
```

## Required tools
Other than the repository and Python, it is required to have the following tools installed, to work with Kubernetes:

* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
* [helm](https://helm.sh/docs/intro/install/)

## Kubernetes cluster

SKAMPI is meant to be deployed - using an Helm chart - to a Kubernetes cluster. Usually, we can use the [CI/CD](#deployment-using-gitlab) pipelines to deploys such environments. Although, if you have the compute resources, you can setup a local cluster with [minikube](https://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-minikube/). This repository will create a single-node Kubernetes cluster, and add all the required add-ons (i.e., ingress, loadbalancers, etc). It is recomended to also deploy the [SKA Tango Operator](https://developer.skao.int/projects/ska-tango-operator/en/latest/README.html#installation) to improve the deployment of the software components.

Afterwards, the process of deploying SKAMPI is the same, regardless of the cluster.

>If you need access to a cluster, please reach out to [#team-system-support](https://skao.slack.com/archives/CEMF9HXUZ) to ask for access. Bear in mind that this access might not be possible. For the existing datacentres, the CI/CD pipeline is the preferred way of deployment.

## SKA Tango Examples and Tango Images
If your component or product is ready for integration, skip the following sections and go to [Development](#development).

A basic understanding of the [SKA Tango Examples](https://gitlab.com/ska-telescope/ska-tango-examples/) repository is required before attempting to integrate a component on SKAMPI. Please clone the repository and base your development on the examples given there. Pay particular attention to how deployment and partial integration is demonstrated using Helm. It will be helpful to follow the SKAMPI [documentation about Helm](https://developer.skao.int/projects/ska-skampi-deployment/en/latest/deployment/helm.html) while you are doing this. There are also links to the documentation on **container orchestration** which you should also follow.

The SKA Tango Base and Tango Util Helm charts are required by most of the deployments that are integrated in SKAMPI. It is therefore also worth your while to look at the [SKA Tango Images](https://gitlab.com/ska-telescope/ska-tango-images/) repository. The deployment workflow is very similar to SKAMPI.

## Helm Charts
Helm - the Kubernetes release manager - uses Charts as its deployable unit. From a set of **templates** and configuration **values** included in the Chart, it is capable of generating complex YAML manifests (applicable to Kubernetes clusters) that allow us to run and manage applications.

That enables a large project such as the SKA to configure a set of standard Kubernetes resources, that ultimately deploy the telescope software. A Chart, similar to any other binary, can be published to a repository, and re-used. In SKAMPI we make use of **umbrella charts**, which are compositions of other charts (telescope subsystems).

For an understanding of how Helm Charts are used in the SKAMPI project, please go to the section on [Templating the Application](https://developer.skao.int/en/latest/tools/containers/orchestration-guidelines.html#templating-the-application) under the [Container Orchestration Guidelines](https://developer.skao.int/en/latest/tools/containers/orchestration-guidelines.html) of the [Developer Portal](https://developer.skao.int/en/latest/).

# Deployment

To deploy SKAMPI, we can make use of the generic Make targets available. There some required variables we need to set:

* `CONFIG` - Select the desired telescope using `mid` or `low`
* `KUBE_NAMESPACE` - The kubernetes namespace were to install the umbrella chart
* `VALUES` - Path to the values files used to modify the default chart values for deployment (defaults to `pipeline.yaml`)

>Note: To avoid making unintended changes to `pipeline.yaml` it is advised to create a copy of it - `values.yaml` - with the changes, and set `VALUES=values.yaml`

Other variables are also needed. To know all the available variables, please check the **Makefile**:

* `ARCHIVER_DB_PWD` - Password for the archiver database
* `EXPOSE_All_DS` - Expose all device servers to the network (via metallb)
* `ITANGO_ENABLED` -  Deploy ITango
* `ARCHIVER_ENABLED` -  Deploy Archiver
* `OET_INGRESS_ENABLED` -  Depoy Observation Execution Tool (OET) ingress
* `ODA_DEPLOYMENT_ENABLED` -  Deploy Operations Data Archive (ODA)
* `TARANTA_AUTH_DASHBOARD_ENABLED` -  Deploy Taranta

These variables can be set as follows:
* Inline arguments to the make target - `CONFIG=mid make ...`
* Exported environment variables - `export CONFIG=mid; make ...`
* Variables in `PrivateRules.mak` file

Vital part is to select the corrent KUBECONFIG. After setting it, we can run some **kubectl** command to verify we are accessing the correct cluster:

```
kubectl get nodes
```

To install SKAMPI, one can run:

```
make k8s-install-chart
make k8s-wait
```

Once these prerequisites are installed, you can follow the guidelines on [Deployment](https://developer.skao.int/projects/ska-skampi-deployment/en/latest/deployment.html). Note that a partial deployment of SKAMPI is made possible by setting the `<subchartname>.enabled` value to `false` in the values file you are using, for any component (represented by a subchart) that can be switched off to save resources. Details can be found in the Deployment guidelines.

### Environment Settings
To check what some of the most commonly used variables are that your Makefile will use when you run any commands (defaults or environment specific), you can run:

```
$ make vars
```

This should give you all the basic environment variables needed to run the `make` commands as they are called in CI jobs, in case you want to debug deployment issues. To know how the deployment and destruction of an environment is done, check the scripts in the [CI/CD pipelines](https://gitlab.com/ska-telescope/ska-skampi-deployment/-/blob/main/.gitlab/ci/common.gitlab-ci.yml).

### Deployment using Gitlab

Installation/Deployment of SKAMPI is much simpler using the Gitlab CI Pipelines (and this is the recommended method), as everything required to set up the environment is included in the CI infrastructure. You can deploy **on-demand** environments from branches only, so you will need to create a branch.

Make sure you follow [SKA's branching policies and best-practices](https://developer.skao.int/en/latest/tools/git.html) when creating the branches.

```
$ git checkout -b <branch>
$ git push --set-upstream origin <branch>
```

After pushing the branch, a new [pipeline](https://gitlab.com/ska-telescope/ska-skampi-deployment/-/pipelines) will be created - also for each commit - that we can use do deploy an on-demand environment. In the on-demand section, separated by datacentre, we can do the following:

* **deploy** - Deploys the environment to a namespace named after the gitlab pipeline. This is important as in the same pipeline, each job run will target the **same** namespace
* **destroy** - Undeploys SKAMPI and removes the namespaces, along with their resources
* **info** - Collects information on the contents of the namespace (i.e., pods, images, etc)
* **redeploy** - Undeploys and deploys a fresh environment, in the same namespace
* **test** - Launches a test pipeline against the on-demand environment. The scope of the tests can be changed via environment variables of the job. To know more, check the [testing](https://gitlab.com/ska-telescope/ska-skampi#running-tests) documentation

Follow the [Documentation](https://developer.skao.int/projects/ska-skampi/en/latest/deployment/multitenancy.html#deploying-in-a-namespace-linked-to-a-development-branch) for deploying in a namespace, and then downloading and using the KUBECONFIG file. This file is your key to accessing the namespace in the cluster where your branch has just been deployed.

# Development

Developing SKAMPI charts should be comprised of merely changing the versions of the charts that are listed as dependencies in the umbrella charts. Also, changing values (for instance, of the underlying TANGO runtime image for the device servers) might be needed, regardless of the version of the charts.

To know if there are new versions of the dependent charts, run:

```
make helm-check-deps
```

This will search the production repositories (for SKA charts, https://artefact.skao.int/) for new versions of a given chart.

## Adding a new product/component

This is an example of how the deployment would look, if a new application ("Application three"), were to be added to the minimal deployment described in the section on [Modifying deployment configuration](#modifying-deployment-configuration):

```{mermaid}
flowchart TB
    subgraph "Namespace integration-mid"
      subgraph "SKAMPI Landing Page"
        d1[chart ska-landingpage] --> d2(container ska-landingpage)
      end
      subgraph SKA Mid Chart
        subgraph "Tango Util Library Chart"
          a1[chart ska-tango-util]--> |uses| a2(container ska-tango-util)
        end
        subgraph "SKA Tango Base application"
          b1[chart ska-tango-base]-->b2(container databaseds)
          b1--> |uses| a2
        end
        subgraph "Application three"
          c1[chart c1]-->c2(container c2)
          c1--> |uses| a2
        end
      end
    end
```

### Verifying Chart versions deployed by Helm
The landing page holds a list of versions of the Charts that are deployed. This list is generated at deploy-time, taking into account the enabled and disabled items. This should give an indication of what should be deployed. 

>Note: This is not a list of successfully deployed items, but merely a list of items that should be expected to run. Further investigation is required if subsystems are unexpectedly not functioning.

For the above deployment, when you click on `About >> Version`, you'll see only the three sub-charts that were deployed, the umbrella chart (in SKAMPI we only have Mid and Low umbrella charts), and their versions, for example:
![](_static/img/about_version.png)

# Subsystems

## EDA 
The EDA solution is based on HDB++ archiver with TimescaleDB as the backend database. The HDB++ Configuration Manager configures the attributes to be archived and defines which Event Subscriber is responsible for a set of Tango attributes to be archived. For more detail information on EDA please visit the [archiver](https://ska-tango-archiver.readthedocs.io/en/latest/) documentation.

## Troubleshooting / FAQ
Finding issues with SKAMPI deployments can sometimes be difficult, and knowledge of Kubernetes and Tango are essential. Some excellent troubleshooting tips for Kubernetes can be found at https://kubernetes.io/docs/tasks/debug-application-cluster/troubleshooting.

## Getting Help
If you get stuck, the SKA Slack channels related to the technology that you are working on, are good places to go. These are a few useful channels:

### [#help-kubernetes](https://skao.slack.com/archives/C016W494EHZ)
For help with Kubernetes related issues.

### [#help-gitlab](https://skao.slack.com/archives/C016F65FBB9)
Struggling with CI or other Gitlab related things? Go here.

### [#proj-mvp](https://skao.slack.com/archives/CKBDRGCKB)
This is the channel where (in general) the current progress of the MVP is discussed, as well as integration issues on SKAMPI.

### [#team-system-support](https://skao.slack.com/archives/CEMF9HXUZ)
The System Team help out whenever there are CI related problems that seem to be out of developers' control.
