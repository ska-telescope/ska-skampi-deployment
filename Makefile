.DEFAULT_GOAL := help
.PHONY: help

MINIKUBE ?= false
LOADBALANCER_IP ?= 10.100.20.1# stfc-techops ingress lb
INGRESS_PROTOCOL ?= https
ifeq ($(strip $(MINIKUBE)),true)
LOADBALANCER_IP ?= $(shell minikube ip)
INGRESS_HOST ?= $(LOADBALANCER_IP)
INGRESS_PROTOCOL ?= http
endif

CLUSTER_DOMAIN ?= cluster.local
KUBE_NAMESPACE ?= integration
KUBE_NAMESPACE_SDP ?= $(KUBE_NAMESPACE)-sdp
HELM_RELEASE ?= test
VALUES ?= values.yaml
KUBE_HOST ?= $(LOADBALANCER_IP)

ALLOWED_CONFIGS ?= mid low
CONFIG ?=

-include .make/base.mk

K8S_CHART ?= ska-$(CONFIG)
K8S_EXTRA_CHART_PARMS ?=
ifdef CONFIG
	HELM_CHARTS_TO_PUBLISH = ska-$(CONFIG) ska-landingpage
else
	HELM_CHARTS_TO_PUBLISH = ska-mid ska-low ska-landingpage
endif

KUBE_APP ?= ska-skampi
KEEP_NAMESPACE ?= false

ifdef CI_COMMIT_REF_NAME
	GIT_BRANCH = $(CI_COMMIT_REF_NAME)
else
	GIT_BRANCH = $(shell git rev-parse --abbrev-ref HEAD)
endif
ifdef CI_COMMIT_REF_SLUG
	GIT_BRANCH_SLUG = $(CI_COMMIT_REF_SLUG)
else
	GIT_BRANCH_SLUG = $(shell echo $(GIT_BRANCH) | tr '[:upper:]' '[:lower:]' | tr '-' '_'| tr '.' '_')
endif


# Deployment variables
XAUTHORITYx ?= ${XAUTHORITY}
TANGO_DATABASE_DS ?= databaseds-tango-base
TANGO_HOST ?= $(TANGO_DATABASE_DS).$(KUBE_NAMESPACE).svc.$(CLUSTER_DOMAIN):10000
ARCHIVER_TIMESCALE_HOST_NAME ?= timescaledb.ska-eda-$(CONFIG)-db.svc.$(CLUSTER_DOMAIN)
TANGO_SERVER_PORT ?= 45450
ARCHIVER_DBNAME ?= $(shell echo $(CONFIG)_archiver_db_$(GIT_BRANCH_SLUG) | head -c 59)
# Sanitize ARCHIVER_DBNAME: Postgres DB name maxes at 59 chars
ARCHIVER_DBNAME := $(shell echo $(ARCHIVER_DBNAME) | tr '-' '_'| tr '.' '_' | head -c 59)
ARCHIVER_PORT ?= 5432
ARCHIVER_DB_USER ?= admin
ARCHIVER_DB_PWD ?=
TELESCOPE_ENVIRONMENT ?= "$(CONFIG)-STFC"
EXPOSE_All_DS ?= false
CONFIG_MANAGER= $(CONFIG)-eda/cm/01
ARCHWIZARD_VIEW_DBNAME = SKA_ARCHIVER
ARCHWIZARD_CONFIG ?= $(ARCHWIZARD_VIEW_DBNAME)=tango://$(TANGO_HOST)/$(CONFIG_MANAGER)
ODA_URI ?= http://ska-db-oda-rest-$(HELM_RELEASE).$(KUBE_NAMESPACE).svc.$(CLUSTER_DOMAIN):5000/$(KUBE_NAMESPACE)/api/v1

# Deployment flags
ITANGO_ENABLED ?= false
ARCHIVER_ENABLED ?= true
OET_INGRESS_ENABLED ?= true
ODA_DEPLOYMENT_ENABLED ?= true
TARANTA_AUTH_DASHBOARD_ENABLED ?= false

SECRET_VARS ?= ARCHIVER_DB_PWD
SHOW_VARS ?= KUBE_NAMESPACE KUBE_NAMESPACE_SDP HELM_RELEASE VALUES CLUSTER_DOMAIN \
	CONFIG XAUTHORITYx TANGO_HOST TANGO_SERVER_PORT \
	ITANGO_ENABLED ARCHIVER_ENABLED OET_INGRESS_ENABLED ODA_DEPLOYMENT_ENABLED \
	ARCHIVER_DBNAME ARCHIVER_TIMESCALE_HOST_NAME ARCHIVER_PORT ARCHIVER_DB_USER ARCHIVER_DB_PWD \
	TELESCOPE_ENVIRONMENT EXPOSE_All_DS ARCHWIZARD_CONFIG ODA_URI

VARS := \
	$(foreach var,$(SHOW_VARS),\
		$(if $(filter $(var),$(SECRET_VARS)),\
			$(if $($(var)),\
				VAR_$(var)=***,\
				VAR_$(var)=\
			),\
			VAR_$(var)=$($(var))\
		)\
	)

# Run only for k8s targets
ifneq ($(findstring k8s-,$(firstword $(MAKECMDGOALS))),)

ifeq ($(filter $(CONFIG),mid low),)
$(error `CONFIG` must be one of $(ALLOWED_CONFIGS))
endif

ifeq ($(strip $(KUBE_NAMESPACE)),)
$(error `KUBE_NAMESPACE` must be provided)
endif

endif

# Some environments need HTTP(s) requests to go through a proxy server. If http_proxyç
# is present we assume all proxy vars are set and pass them through. See
# https://about.gitlab.com/blog/2021/01/27/we-need-to-talk-no-proxy/ for some
# background reading about these variables.
ifneq ($(http_proxy),)
no_proxy ?= landingpage,oet-rest-$(HELM_RELEASE),.svc.$(CLUSTER_DOMAIN),${no_proxy}
SDP_PROXY_VARS = \
	--set ska-sdp.proxy.server=${http_proxy} \
	--set "ska-sdp.proxy.noproxy={${no_proxy}}"
endif

K8S_CHART_PARAMS = \
	--set ska-tango-base.xauthority="$(XAUTHORITYx)" \
	--set global.minikube=$(MINIKUBE) \
	--set global.tango_host=$(TANGO_DATABASE_DS):10000 \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	--set global.device_server_port=$(TANGO_SERVER_PORT) \
	--set global.labels.app=$(KUBE_APP) \
	--set ska-tango-base.itango.enabled=$(ITANGO_ENABLED) \
	--set ska-sdp.helmdeploy.namespace=$(KUBE_NAMESPACE_SDP) \
	--set ska-tango-archiver.hostname=$(ARCHIVER_TIMESCALE_HOST_NAME) \
	--set ska-tango-archiver.enabled=$(ARCHIVER_ENABLED) \
	--set ska-tango-archiver.dbname=$(ARCHIVER_DBNAME) \
	--set ska-tango-archiver.port=$(ARCHIVER_PORT) \
	--set ska-tango-archiver.dbuser=$(ARCHIVER_DB_USER) \
	--set ska-tango-archiver.dbpassword=$(ARCHIVER_DB_PWD) \
	--set ska-tango-archiver.telescope_environment=$(TELESCOPE_ENVIRONMENT)\
	--set global.exposeAllDS=$(EXPOSE_All_DS) \
	--set ska-tango-archiver.archwizard_config=$(ARCHWIZARD_CONFIG) \
	--set ska-sdp.ska-sdp-qa.zookeeper.clusterDomain=$(CLUSTER_DOMAIN) \
	--set ska-sdp.ska-sdp-qa.kafka.clusterDomain=$(CLUSTER_DOMAIN) \
	--set ska-sdp.ska-sdp-qa.redis.clusterDomain=$(CLUSTER_DOMAIN) \
	--set ska-oso-oet.rest.ingress.enabled=$(OET_INGRESS_ENABLED) \
	$(SDP_PROXY_VARS) \
	$(K8S_EXTRA_CHART_PARMS)

ifeq ($(strip $(ODA_DEPLOYMENT_ENABLED)),true)
K8S_CHART_PARAMS += \
	--set ska-db-oda.enabled=true \
	--set ska-oso-oet.rest.oda.url=$(ODA_URI) \
	--set ska-db-oda.rest.backend.type=filesystem \
	--set ska-db-oda.pgadmin4.enabled=false \
	--set ska-db-oda.postgresql.enabled=false
endif

ifeq ($(strip $(MINIKUBE)),true)
ifeq ($(strip $(TARANTA_AUTH_DASHBOARD_ENABLE)),true)
K8S_CHART_PARAMS += \
	--set ska-taranta.enabled=true \
	--set ska-taranta.tangogql.replicas=1 \
	--set global.taranta_auth_enabled=true \
	--set global.taranta_dashboard_enabled=true
else
K8S_CHART_PARAMS += --set ska-taranta.enabled=false
endif
else
K8S_CHART_PARAMS += --set ska-taranta.enabled=true
ifeq ($(strip $(TARANTA_AUTH_DASHBOARD_ENABLE)),true)
K8S_CHART_PARAMS += \
	--set global.taranta_auth_enabled=true \
	--set global.taranta_dashboard_enabled=true
endif
endif

# add `--values <file>` for each space-separated file in VALUES that exists
ifneq (,$(wildcard $(VALUES)))
	K8S_CHART_PARAMS += $(foreach f,$(wildcard $(VALUES)),--values $(f))
endif

-include .make/k8s.mk
-include .make/helm.mk
-include PrivateRules.mak

vars:
	$(info ##### SKAMPI deploy vars)
	@echo "$(VARS)" | sed "s#VAR_#\n#g"

links:
	@echo ${CI_JOB_NAME}
	@echo "############################################################################"
	@echo "#            Access the Skampi landing page here:"
	@echo "#            $(INGRESS_PROTOCOL)://$(INGRESS_HOST)/$(KUBE_NAMESPACE)/start/"
	@echo "#     NOTE: Above link will only work if you can reach $(INGRESS_HOST)"
	@echo "############################################################################"
	@if [[ -z "${LOADBALANCER_IP}" ]]; then \
		exit 0; \
	elif [[ $(shell curl -I -s -o /dev/null -I -w \'%{http_code}\' http$(S)://$(LOADBALANCER_IP)/$(KUBE_NAMESPACE)/start/) != '200' ]]; then \
		echo "ERROR: http://$(LOADBALANCER_IP)/$(KUBE_NAMESPACE)/start/ unreachable"; \
		exit 10; \
	fi

k8s-pre-install-chart:
	@echo "k8s-pre-install-chart: creating the SDP namespace $(KUBE_NAMESPACE_SDP)"
	@make k8s-namespace KUBE_NAMESPACE=$(KUBE_NAMESPACE_SDP)

k8s-pre-install-chart-car:
	@echo "k8s-pre-install-chart-car: creating the SDP namespace $(KUBE_NAMESPACE_SDP)"
	@make k8s-namespace KUBE_NAMESPACE=$(KUBE_NAMESPACE_SDP)

k8s-pre-uninstall-chart:
	@echo "k8s-post-uninstall-chart: deleting the SDP namespace $(KUBE_NAMESPACE_SDP)"
	@if [ "$(KEEP_NAMESPACE)" != "true" ]; then make k8s-delete-namespace KUBE_NAMESPACE=$(KUBE_NAMESPACE_SDP); fi
